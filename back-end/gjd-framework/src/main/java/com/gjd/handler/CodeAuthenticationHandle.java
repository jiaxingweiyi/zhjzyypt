package com.gjd.handler;

import cn.hutool.core.util.ObjectUtil;
import com.gjd.domain.ResponseResult;
import com.gjd.domain.dto.CodeDto;
import com.gjd.domain.entity.*;
import com.gjd.domain.vo.AdminInfoVo;
import com.gjd.domain.vo.AdminLoginVo;
import com.gjd.domain.vo.UserInfoVo;
import com.gjd.domain.vo.UserLoginVo;
import com.gjd.domain.vo.enums.AppHttpCodeEnum;
import com.gjd.mapper.UserMapper;
import com.gjd.utils.BeanCopyUtils;
import com.gjd.utils.JwtUtil;
import com.gjd.utils.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 验证码过滤器，用于处理手机验证码和邮箱验证码两种方式
 */
@Component
public class CodeAuthenticationHandle {
    @Autowired
    private RedisCache redisCache;

    @Autowired
    private UserMapper userMapper;
    public ResponseResult UserCodeAuth(CodeObject data){
        String code = null;
        User userInfo = null;
        CodeDto cacheObject_1 = redisCache.getCacheObject("userTelephone:" + data.getObject());
        CodeDto cacheObject_2 = redisCache.getCacheObject("userEmail:" + data.getObject());
        if (ObjectUtil.isNotEmpty(cacheObject_1)){
            userInfo = (User) cacheObject_1.getObject();
            code = cacheObject_1.getCode();
            redisCache.deleteObject("userTelephone:" + data.getObject());
        }
        else if (ObjectUtil.isNotEmpty(cacheObject_2)){
            userInfo = (User) cacheObject_2.getObject();
            code = cacheObject_2.getCode();
            redisCache.deleteObject("userEmail:" + data.getObject());
        }
        if (data.getCode().equals(code)){
            // 验证码通过
            String jwt = JwtUtil.createJWT(String.valueOf(userInfo.getId()));
            // 相当于执行了自定义UserDetailImpl的方法
            LoginUser loginUser = new LoginUser(userInfo,null);
            // 把用户信息存入redis中，设置为3天就登录过期
            redisCache.setCacheObject("loginUser:" + String.valueOf(userInfo.getId()), loginUser);
            // 将用户信息和token存入userInfoVo返回客户端
            UserInfoVo userInfoVo = BeanCopyUtils.copyBean(loginUser.getUser(), UserInfoVo.class);
            UserLoginVo userLoginVo = new UserLoginVo(jwt, userInfoVo);
            return ResponseResult.okResult(userLoginVo);
        }
        // TODO:状态码未指定
        return ResponseResult.errorResult(AppHttpCodeEnum.CODE_FAIL);
    }

    public ResponseResult AdminCodeAuth(CodeObject data){
        String code = null;
        Admin adminInfo = null;
        CodeDto cacheObject_1 = redisCache.getCacheObject("AdminTelephone:" + data.getObject());
        CodeDto cacheObject_2 = redisCache.getCacheObject("AdminEmail:" + data.getObject());
        if (ObjectUtil.isNotEmpty(cacheObject_1)){
            adminInfo = (Admin) cacheObject_1.getObject();
            code = cacheObject_1.getCode();
            redisCache.deleteObject("AdminTelephone:" + data.getObject());
        }
        else if (ObjectUtil.isNotEmpty(cacheObject_2)){
            adminInfo = (Admin) cacheObject_2.getObject();
            code = cacheObject_2.getCode();
            redisCache.deleteObject("AdminEmail:" + data.getObject());
        }
        if (data.getCode().equals(code)){
            // 验证码通过
            String jwt = JwtUtil.createJWT(String.valueOf(adminInfo.getId()));
            // 相当于执行了自定义UserDetailImpl的方法
            LoginAdmin loginAdmin = new LoginAdmin(adminInfo,null);
            // 把管理员信息存入redis中，设置为7天就登录过期
            redisCache.setCacheObject("loginAdmin:" + String.valueOf(adminInfo.getId()), loginAdmin);
            // 将管理员信息和token存入userInfoVo返回客户端
            AdminInfoVo adminInfoVo = BeanCopyUtils.copyBean(loginAdmin.getAdmin(), AdminInfoVo.class);
            AdminLoginVo adminLoginVo = new AdminLoginVo(jwt, adminInfoVo);
            return ResponseResult.okResult(adminLoginVo);
        }
        // TODO:状态码未指定
        return ResponseResult.errorResult(AppHttpCodeEnum.CODE_FAIL);
    }
}