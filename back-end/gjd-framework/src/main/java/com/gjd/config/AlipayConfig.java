package com.gjd.config;

/**
 * @author Mr.M
 * @version 1.0
 * @description 支付宝配置参数
 * @date 2022/10/20 22:45
 */
public class AlipayConfig {
    // 商户appid
    public static String APPID = "2021000122607473";
    // 私钥 pkcs8格式的
    public static String RSA_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCGHn6aLnuB27oMVdRt31L72n582XI8F7exvqdf/7Dq2FwDi68iHBv0U3uTfj4UBZXF+mNpi5DcPQJKMZHjb3+MZa4eVP7Gba7gIQ7/hQaqkyxQVCPnBrZbOFAKHiaYHCcbRZ3lxlJ5jkfxbIWuQXPNgDALwMOw6u2l01BrnV4jQDpIomc+uNLtVqTzNZvukbssYP1gI/Ndm5i2WLrg3zbswKSXYY4ssM+ixnPozt7/JBP+KkjNxqm2Rr5UmaYaqHT08H9xiqX6VwKfNLKuva34aSK2dh/+IsXGl6lNuMrWo7anZf/rXKZkZrbuW2KDkC0rhh8pgpuhHlIjqLW07iANAgMBAAECggEAFHNH6KiXyog/iSv5NTutQpvhV5Ysdf9D0kV47gll55lQBnZhIPlaRdoI6MeCHblwSfzQ9cIDN6HR3JBMZ3qT30uNzjA1nPoShThlOg8FojLIugLqaoq6eZRPmAta5Sy9xcnKtsOTxdctHyHmDFkMmFQof1JIRIy96eAbzhzzwroK5WV7TCZGkgaWg+VNq5M1uUbMNIM2s1f3NHd7f00ECnyATBPaDQktBrDahANmqDEBpzzmwLK8PlQF6CSsEAQWIPmvaoJrpRLcVOZUvCdnA8GP2SwRBDrxjBiIe8e6fhybnkhCq41L7Wu9GEErJwLTQbG6AZ80aUGP/dpku1RtXQKBgQC8DpWNO/5HT0KRHGmrLVk3b7lL3MVkaGUK4b7TRsaNs7VzoXZ4tKk0E3r/mWY5bk5y7Yun/iG/NLzlOtCK5mVNkp37eCzRDhLXywaKYq2uAlzslLRugNBeReFAlVEYfGLpmMR0mO//c2xrkfDUr8uK6P4B+0TVjM6/+dIylD7euwKBgQC2kzDg1aBe+7vIxCA1faTLP+8AW3Imk30+1t0TagYhaMgaepb6Fg+ZTwxNBARwFgFPzG+lJprF2FnO86a4wy5SoCSmPvLzI7DG5j/k9fUFFKV30rOs7ASbvJruilat7LvgYDotVacz0lJUey022Yhfo9Q7h2lzk+oXi5yZx/Cj1wKBgQCANslL051X7PgLi32X0YFERI3m5QxHms1ZD/RUg6qXrfCGd40YWsQJ10ZCwclqWoRt4vsWfDNx83jeKK1KJi7nJpFDg/m6RogeTDOXYhPb0YydV/10Yaquu6IJsEPsF9IwRZGblTH7ziwbYxL5TynM+XFE1JVnq2rChIcFKQF8RQKBgQCCJUh5bS7lpnLE+bGwD8FyNT/stEMZ5pcoPUfYeAKovCWwYmTPHvy9n1C4cb2sHLOQq72GL4+yqrfSQ3M8T73snkWh0EulMHLqReWUFBQ2K/KMCMGV6aSbU4Xptul1D8UZe97cxCrtUObpW+juKYECFCnMyYlNH0qGwDnAkXsjfwKBgD1pHWke6vX6aTT52TtuyK9iXh7iSLVOQlH2QjzmVqNuCoz4swkwe3mAwgMIHndzCMUeds9IIsu1PK0ydi/zn9FDLG2rPlhBTlpeyU785T+erke++6nOwujAAhweqSy9zxNqwjQheN8Ze1cG2fDQNW0Eh3u+iPeiW0N2SKIx6Qxm";
    // 服务器异步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://商户网关地址/alipay.trade.wap.pay-JAVA-UTF-8/notify_url.jsp";
    // 页面跳转同步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问 商户可以自定义同步跳转地址
    public static String return_url = "http://商户网关地址/alipay.trade.wap.pay-JAVA-UTF-8/return_url.jsp";
    // 请求网关地址
    public static String URL = "https://openapi.alipaydev.com/gateway.do";
    // 编码
    public static String CHARSET = "UTF-8";
    // 返回格式
    public static String FORMAT = "json";
    // 支付宝公钥
    public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmhFO6mbeUTNmNKqhF9sQVmnhbKx//E09lOHO6owbt5hkvSqAr/4U+61ghuutPoz2q7csA19xsCX9+OYknsLuu7r4/nfjx1hq1F3ApWwLWTTK4anvat8+fmW/4W0T14KuGOgAenMS0b8wc1E39UfKmGfEjtONOHSM1gOzAifeqbLe7LAayGGKhfAQ99+VC1+jP+3Xskfw3e4XGeGH0LH46cEaNMqDz1RGUT3fgTCzb/tOoKyoR2kx4o+IxiiVhz08fOLI1PO4YYXFsj/FLJi+xg+c5lGauP7NjUDILU3KXYy4EJkuFot77+ZZryzujbpyE+oNst89k/d5whWfU2I2HQIDAQAB";
    // 日志记录目录
    public static String log_path = "/log";
    // RSA2
    public static String SIGNTYPE = "RSA2";
}
