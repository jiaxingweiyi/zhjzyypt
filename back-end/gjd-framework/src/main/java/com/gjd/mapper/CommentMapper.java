package com.gjd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gjd.domain.entity.Comment;
import org.apache.ibatis.annotations.Mapper;

/**
 * 评论表(Comment)表数据库访问层
 *
 * @author makejava
 * @since 2023-01-05 15:34:49
 */
@Mapper
public interface CommentMapper extends BaseMapper<Comment> {

}

