package com.gjd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gjd.domain.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {

}
