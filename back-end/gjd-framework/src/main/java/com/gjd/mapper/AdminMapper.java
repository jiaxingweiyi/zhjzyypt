package com.gjd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gjd.domain.entity.Admin;
import org.apache.ibatis.annotations.Mapper;

/**
* @author yang'shun'tao
* @description 针对表【admin(用户表)】的数据库操作Mapper
* @createDate 2023-01-11 22:15:56
* @Entity generator.domain/entity.Admin
*/
@Mapper
public interface AdminMapper extends BaseMapper<Admin> {


}
