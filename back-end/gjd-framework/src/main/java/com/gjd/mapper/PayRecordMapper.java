package com.gjd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gjd.domain.entity.PayRecord;
import org.apache.ibatis.annotations.Mapper;

/**
* @author yang'shun'tao
* @description 针对表【pay_record(支付记录表)】的数据库操作Mapper
* @createDate 2023-01-16 15:55:59
* @Entity com.gjd.domain.entity.PayRecord
*/
@Mapper
public interface PayRecordMapper extends BaseMapper<PayRecord> {

}




