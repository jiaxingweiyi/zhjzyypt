package com.gjd.mapper;

import com.gjd.domain.entity.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author yang'shun'tao
* @description 针对表【orders(订单表)】的数据库操作Mapper
* @createDate 2023-01-16 21:39:20
* @Entity generator.domain.Orders
*/
@Mapper
public interface OrdersMapper extends BaseMapper<Orders> {
}




