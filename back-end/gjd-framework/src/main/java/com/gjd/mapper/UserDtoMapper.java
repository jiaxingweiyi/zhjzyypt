package com.gjd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gjd.domain.dto.UserDto;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDtoMapper extends BaseMapper<UserDto> {
}
