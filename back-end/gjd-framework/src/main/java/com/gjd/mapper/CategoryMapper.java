package com.gjd.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gjd.domain.entity.Category;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 分类表(Category)表数据库访问层
 *
 * @author makejava
 * @since 2023-01-10 21:22:38
 */
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {
}

