package com.gjd.mapper;

import com.gjd.domain.entity.ServerCondition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author yang'shun'tao
* @description 针对表【server_condition】的数据库操作Mapper
* @createDate 2023-01-15 10:07:08
* @Entity com.gjd.domain.entity.ServerCondition
*/
@Mapper
public interface ServerConditionMapper extends BaseMapper<ServerCondition> {

}




