package com.gjd.service;


import com.gjd.domain.ResponseResult;
import com.gjd.domain.dto.UserDto;
import com.gjd.domain.entity.User;

// 搜索功能
public interface UserSurfService {
    ResponseResult surf(String field,String token,Long page,Long size);

    ResponseResult selectUserByConditonPage(UserDto userDto, Long pageNum, Long pageSize);
}