package com.gjd.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gjd.domain.ResponseResult;
import com.gjd.domain.entity.Admin;
import com.gjd.domain.entity.User;
import com.gjd.domain.vo.enums.AppHttpCodeEnum;
import com.gjd.mapper.UserMapper;
import com.gjd.service.AdminService;
import com.gjd.mapper.AdminMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author yang'shun'tao
* @description 针对表【admin(用户表)】的数据库操作Service实现
* @createDate 2023-01-11 22:15:56
*/
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService{


    @Autowired
    private UserMapper userMapper;

    @Override
    public ResponseResult updateUserInfo(User user) {
        int res = userMapper.updateById(user);
        if (res == 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.SYSTEM_ERROR);
        }
        return ResponseResult.okResult();
    }
}
