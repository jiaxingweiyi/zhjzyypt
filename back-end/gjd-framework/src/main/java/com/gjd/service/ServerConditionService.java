package com.gjd.service;

import com.gjd.domain.ResponseResult;
import com.gjd.domain.entity.ServerCondition;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.bind.annotation.RequestBody;

/**
* @author yang'shun'tao
* @description 针对表【server_condition】的数据库操作Service
* @createDate 2023-01-15 10:07:08
*/
public interface ServerConditionService extends IService<ServerCondition> {

    ResponseResult refineCondition(@RequestBody ServerCondition serverCondition);

    ResponseResult getCondition(Long serverId);
}
