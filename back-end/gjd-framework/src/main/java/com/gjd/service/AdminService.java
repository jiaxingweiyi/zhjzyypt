package com.gjd.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gjd.domain.ResponseResult;
import com.gjd.domain.entity.Admin;
import com.gjd.domain.entity.User;

/**
* @author yang'shun'tao
* @description 针对表【admin(用户表)】的数据库操作Service
* @createDate 2023-01-11 22:15:56
*/
public interface AdminService extends IService<Admin> {

    ResponseResult updateUserInfo(User user);
}
