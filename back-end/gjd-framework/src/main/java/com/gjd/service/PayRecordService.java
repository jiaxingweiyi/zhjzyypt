package com.gjd.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gjd.domain.entity.PayRecord;

/**
* @author yang'shun'tao
* @description 针对表【pay_record(支付记录表)】的数据库操作Service
* @createDate 2023-01-16 15:55:59
*/
public interface PayRecordService extends IService<PayRecord> {

}
