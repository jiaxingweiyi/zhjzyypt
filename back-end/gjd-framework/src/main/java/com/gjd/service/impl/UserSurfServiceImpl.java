package com.gjd.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gjd.domain.ResponseResult;
import com.gjd.domain.dto.UserDto;
import com.gjd.domain.entity.Category;
import com.gjd.domain.entity.ServerCondition;
import com.gjd.domain.entity.User;
import com.gjd.domain.vo.PageVo;
import com.gjd.domain.vo.UserInfoVo;
import com.gjd.mapper.CategoryMapper;
import com.gjd.mapper.ServerConditionMapper;
import com.gjd.mapper.UserDtoMapper;
import com.gjd.mapper.UserMapper;
import com.gjd.service.UserSurfService;
import com.gjd.utils.BeanCopyUtils;
import org.omg.CORBA.NO_MEMORY;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

import static com.gjd.constants.SystemConstants.NORMAL;

@Service("surfServiceImpl")
public class UserSurfServiceImpl implements UserSurfService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserDtoMapper userDtoMapper;

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private ServerConditionMapper serverConditionMapper;

    @Override
    public ResponseResult surf(String field, String token, Long page, Long size) {
        // 查询处理
        LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 使用模糊查询对名字、地区、简介进行匹配，使用准确查询对工号、员工状态进行匹配
        lambdaQueryWrapper.like(User::getWorkRange, field).or()
                .like(User::getWorkExp, field).or()
                .like(User::getSkill, field).or()
                .like(User::getWorkArea, field);
        Page<User> iPage = new Page<>(page, size);
        Page<User> iPage1 = userMapper.selectPage(iPage, lambdaQueryWrapper);
        List<UserInfoVo> list = BeanCopyUtils.copyBeanList(iPage1.getRecords(), UserInfoVo.class);
        PageVo pageVo = new PageVo(list, iPage.getTotal());
        return ResponseResult.okResult(pageVo);
    }

    @Override
    public ResponseResult selectUserByConditonPage(UserDto userDto, Long pageNum, Long pageSize) {

        Long total = 0L;
        LambdaQueryWrapper<UserDto> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(!userDto.getWorkArea().equals(""), UserDto::getWorkArea, userDto.getWorkArea());
        queryWrapper.like(!userDto.getNickName().equals(""), UserDto::getNickName, userDto.getNickName());
        queryWrapper.eq(!userDto.getUserType().equals("0"), UserDto::getUserType, userDto.getUserType());
        queryWrapper.eq(!userDto.getStay().equals(0), UserDto::getStay, userDto.getStay());

        if (userDto.getServerType() != null) {
            if (userDto.getServerType().equals("长期工")) {
                queryWrapper.eq(UserDto::getServerType, userDto.getServerType());
            } else if (userDto.getServerType().equals("短期工")) {
                queryWrapper.eq(UserDto::getServerType, userDto.getServerType());
            }
        }

        if (!userDto.getSalary().equals(0)) {
            queryWrapper.between(userDto.getSalary() == 1, UserDto::getSalary, 0, 100);
            queryWrapper.between(userDto.getSalary() == 2, UserDto::getSalary, 100, 300);
            queryWrapper.between(userDto.getSalary() == 3, UserDto::getSalary, 300, 500);
            queryWrapper.between(userDto.getSalary() == 4, UserDto::getSalary, 500, 1000);
        }

        if (!userDto.getSalaryOrder().equals(0)) {
            if (userDto.getSalaryOrder().equals(1)) {
                queryWrapper.orderByAsc(UserDto::getSalary);
            } else if (userDto.getSalaryOrder().equals(2)) {
                queryWrapper.orderByDesc(UserDto::getSalary);
            }
        }

        if (!userDto.getCategoryName().equals("")) {
            if (!userDto.getCategoryName().equals("不限")) {
                LambdaQueryWrapper<Category> categoryLambdaQueryWrapper = new LambdaQueryWrapper<>();
                categoryLambdaQueryWrapper.eq(Category::getName, userDto.getCategoryName());
                Long categoryId = categoryMapper.selectOne(categoryLambdaQueryWrapper).getId();
                queryWrapper.eq(UserDto::getCategoryId, categoryId);
            }
        }

        Page<UserDto> page = new Page<>(pageNum, pageSize);
        Page<UserDto> userDtoPage = userDtoMapper.selectPage(page, queryWrapper);
        List<UserInfoVo> userInfoVoList = BeanCopyUtils.copyBeanList(userDtoPage.getRecords(), UserInfoVo.class);
        for (int i = 1; i <= userInfoVoList.size(); i++) {
            if (userDtoPage.getRecords().get(i - 1).getStay() == 1) {
                userInfoVoList.get(i - 1).setStay("住家");
            } else if (userDtoPage.getRecords().get(i - 1).getStay() == 2) {
                userInfoVoList.get(i - 1).setStay("不住家");
            }
        }

//        TODO：迭代器删除集合中的元素
        Iterator<UserInfoVo> iterator = userInfoVoList.iterator();
        UserInfoVo userInfoVo;
        while (iterator.hasNext()) {
            userInfoVo = iterator.next();
            LambdaQueryWrapper<Category> categoryLambdaQueryWrapper = new LambdaQueryWrapper<>();
            String categoryName = userInfoVo.getCategoryName();
            categoryLambdaQueryWrapper.eq(Category::getName, categoryName);
            Category category = categoryMapper.selectOne(categoryLambdaQueryWrapper);
            Long serverId = userInfoVo.getId();
            LambdaQueryWrapper<ServerCondition> serverConditionLambdaQueryWrapper = new LambdaQueryWrapper<>();
            serverConditionLambdaQueryWrapper.eq(ServerCondition::getStatus, 0);
            serverConditionLambdaQueryWrapper.eq(ServerCondition::getServerId, serverId);
            if (category == null || serverConditionMapper.selectOne(serverConditionLambdaQueryWrapper) == null) {
                iterator.remove();
            }
        }

        List<UserInfoVo> userInfoVoList2 = BeanCopyUtils.copyBeanList(userInfoVoList, UserInfoVo.class);
        return ResponseResult.okResult(new PageVo(userInfoVoList2, (long) userInfoVoList.size()));
    }
}
