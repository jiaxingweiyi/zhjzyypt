package com.gjd.service;

import com.gjd.domain.ResponseResult;
import com.gjd.domain.entity.Orders;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gjd.domain.entity.PayRecord;
import com.gjd.domain.entity.PayStatus;
import com.gjd.domain.vo.PayRecordVo;

/**
 * @author yang'shun'tao
 * @description 针对表【orders(订单表)】的数据库操作Service
 * @createDate 2023-01-16 11:42:21
 */
public interface PayService extends IService<Orders> {
    ResponseResult operateOrders(Orders orders);

    PayRecord getPayRecordByPayNo(String payNo);

    void saveAliPayStatus(PayStatus payStatus);

}
