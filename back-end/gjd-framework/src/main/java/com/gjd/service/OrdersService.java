package com.gjd.service;

import com.gjd.domain.ResponseResult;
import com.gjd.domain.dto.OrderdDto;
import com.gjd.domain.entity.Orders;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gjd.domain.vo.OrderInfo;

/**
 * @author yang'shun'tao
 * @description 针对表【orders(订单表)】的数据库操作Service
 * @createDate 2023-01-16 21:39:20
 */
public interface OrdersService extends IService<Orders> {

    ResponseResult cancelOrder(Long orderId);

    ResponseResult createOrder(OrderInfo orderInfo);

    ResponseResult createOrder(OrderdDto orderdDto);

    ResponseResult getUserOrder(String orderStatus, Long pageNum, Long pageSize);

    ResponseResult getServerOrder(String orderStatus, Long pageNum, Long pageSize);
}
