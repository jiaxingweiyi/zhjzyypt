package com.gjd.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.gjd.domain.ResponseResult;
import com.gjd.domain.entity.Category;

/**
 * 分类表(Category)表服务接口
 *
 * @author makejava
 * @since 2023-01-10 21:19:05
 */
public interface CategoryService extends IService<Category> {
    ResponseResult getCategoryList();

    ResponseResult getCategoryListPage(Long pageNum, Long pageSize);

    ResponseResult addCategory(Category category);

    ResponseResult updateCategory(Category category);

    ResponseResult deleteCategory(Long category);
}
