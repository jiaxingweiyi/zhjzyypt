package com.gjd.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gjd.domain.ResponseResult;
import com.gjd.domain.entity.Passwords;
import com.gjd.domain.entity.User;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface UserService extends IService<User> {
    ResponseResult register(User user);

    ResponseResult getServerList(Long categoryId, Integer pageNum, Integer pageSize);

    ResponseResult getuserInfo();

    ResponseResult cheackUserInfo(Long serverId);

    ResponseResult uploadImg(MultipartFile img);

    ResponseResult updateUserInfo(User user);

    ResponseResult updatePassword(Passwords passwords);

    ResponseResult getUserListPage(Long pageNum, Long pageSize);

    ResponseResult selectUserListPage(User user, Long pageNum, Long pageSize);

    ResponseResult deleteUser(Long userId);

    ResponseResult checkToken(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
}
