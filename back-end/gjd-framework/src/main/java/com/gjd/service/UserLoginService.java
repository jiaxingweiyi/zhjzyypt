package com.gjd.service;

import com.gjd.domain.ResponseResult;
import com.gjd.domain.entity.CodeObject;
import com.gjd.domain.entity.User;

public interface UserLoginService {
    ResponseResult login(User user);

    ResponseResult loginByEmail(User user);

    ResponseResult logout();

    ResponseResult loginByPhoneAuthCode(User user);
    /**
     * 验证码处理
     * @param object 验证码 + 发送方式
     * @return
     */
    ResponseResult handleCode(CodeObject object);
}
