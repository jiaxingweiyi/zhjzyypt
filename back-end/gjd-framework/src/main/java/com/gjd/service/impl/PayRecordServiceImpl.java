package com.gjd.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gjd.domain.entity.PayRecord;
import com.gjd.service.PayRecordService;
import com.gjd.mapper.PayRecordMapper;
import org.springframework.stereotype.Service;

/**
* @author yang'shun'tao
* @description 针对表【pay_record(支付记录表)】的数据库操作Service实现
* @createDate 2023-01-16 15:55:59
*/
@Service
public class PayRecordServiceImpl extends ServiceImpl<PayRecordMapper, PayRecord>
    implements PayRecordService{

}




