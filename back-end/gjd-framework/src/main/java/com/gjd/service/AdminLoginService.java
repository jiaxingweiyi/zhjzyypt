package com.gjd.service;

import com.gjd.domain.ResponseResult;
import com.gjd.domain.entity.Admin;
import com.gjd.domain.entity.CodeObject;

public interface AdminLoginService {
    ResponseResult login(Admin admin);

    ResponseResult loginByEmail(Admin admin);

    ResponseResult handleCode(CodeObject object);

    ResponseResult logout();

    ResponseResult loginByPhoneAuthCode(Admin admin);
}
