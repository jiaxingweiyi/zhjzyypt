package com.gjd.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gjd.domain.ResponseResult;
import com.gjd.domain.entity.Comment;

/**
 * 评论表(Comment)表服务接口
 *
 * @author makejava
 * @since 2023-01-05 15:34:51
 */
public interface CommentService extends IService<Comment> {

    ResponseResult commentList(Long serverId, Integer pageNum, Integer pageSize);

    ResponseResult addComment(Comment comment);

    ResponseResult myComment(Long serverId, Long pageNum, Long pageSize);
}

