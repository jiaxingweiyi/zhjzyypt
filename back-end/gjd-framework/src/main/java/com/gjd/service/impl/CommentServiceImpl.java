package com.gjd.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gjd.domain.ResponseResult;
import com.gjd.domain.entity.Category;
import com.gjd.domain.entity.Comment;
import com.gjd.domain.entity.ServerCondition;
import com.gjd.domain.entity.User;
import com.gjd.domain.vo.CommentVo;
import com.gjd.domain.vo.PageVo;
import com.gjd.domain.vo.enums.AppHttpCodeEnum;
import com.gjd.exception.SystemException;
import com.gjd.mapper.CommentMapper;
import com.gjd.mapper.ServerConditionMapper;
import com.gjd.mapper.UserMapper;
import com.gjd.service.CommentService;
import com.gjd.utils.BeanCopyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 评论表(Comment)表服务实现类
 *
 * @author makejava
 * @since 2023-01-05 15:34:51
 */
@Service("commentService")
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ServerConditionMapper serverConditionMapper;

    @Override
    public ResponseResult commentList(Long serverId, Integer pageNum, Integer pageSize) {
        LambdaQueryWrapper<Comment> commentLambdaQueryWrapper = new LambdaQueryWrapper<>();
        LambdaQueryWrapper<User> userLambdaQueryWrapper = new LambdaQueryWrapper<>();

        //查询对应serverid的根评论
        commentLambdaQueryWrapper.eq(Comment::getServerId, serverId);
        //根评论为-1
        commentLambdaQueryWrapper.eq(Comment::getRootId, -1);

        //分页查询
        Page<Comment> page = new Page(pageNum, pageSize);
        page(page, commentLambdaQueryWrapper);

        List<CommentVo> commentVoList = toCommentVoList(page.getRecords());

//        //查询所有根评论对应的子评论的集合，并且赋值
//        for (CommentVo commentVo : commentVoList) {
//            List<CommentVo> childrenList = getChildren(commentVo.getId());
//            //赋值
//            commentVo.setChildren(childrenList);
//        }

        userLambdaQueryWrapper.eq(User::getId,serverId);
        User user = userMapper.selectOne(userLambdaQueryWrapper);
        String categoryName = user.getCategoryName();
        for (CommentVo commentVo : commentVoList) {
            commentVo.setCategoryName(categoryName);
        }

        return ResponseResult.okResult(new PageVo(commentVoList, page.getTotal()));
    }

    @Override
    public ResponseResult addComment(Comment comment) {
        //考虑comment的其他字段没有赋值，所以需要手动赋值
        //需要封装获得当前对象信息的工具类--SecurityUtils
        if(!StringUtils.hasText(comment.getContent())){
            throw new SystemException(AppHttpCodeEnum.CONTENT_NOT_NULL);
        }
        save(comment);
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult myComment(Long serverId, Long pageNum, Long pageSize) {
        LambdaQueryWrapper<Comment> commentLambdaQueryWrapper = new LambdaQueryWrapper<>();
        commentLambdaQueryWrapper.eq(Comment::getServerId,serverId);
        Page<Comment> page = new Page<>(pageNum, pageSize);
        page(page, commentLambdaQueryWrapper);
        PageVo commentPageVo = new PageVo(page.getRecords(), page.getTotal());
        return ResponseResult.okResult(commentPageVo);
    }

    private List<CommentVo> getChildren(Long rootid) {
        LambdaQueryWrapper<Comment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Comment::getRootId, rootid);
        queryWrapper.orderByAsc(Comment::getCreateBy);
        List<Comment> commentList = list(queryWrapper);
        //Comment转换为CommentVo
        //TODO: 查询子评论时toCommentVoList报错
        List<CommentVo> commentVoList = toCommentVoList(commentList);
        return commentVoList;
    }

    //从Comment到CommentVo的转换
    private List<CommentVo> toCommentVoList(List<Comment> commentList) {
        List<CommentVo> commentVoList = BeanCopyUtils.copyBeanList(commentList, CommentVo.class);
        //遍历vo集合，手动添加原来没有的字段
        //TODO: 查询用户名称和回复用户名称
        for (CommentVo commentVo : commentVoList) {
            //通过createby查询用户名称并赋值
            String userName = userMapper.selectById(commentVo.getCreateBy()).getNickName();
            commentVo.setUserName(userName);
            //通过toCommentUserId查询用户id并赋值，不为-1才查询
            if(commentVo.getToCommentUserId()!=-1){
                String toCommentUserName = userMapper.selectById(commentVo.getToCommentId()).getNickName();
                commentVo.setToCommentUserName(toCommentUserName);
            }
        }
        return commentVoList;
    }
}

