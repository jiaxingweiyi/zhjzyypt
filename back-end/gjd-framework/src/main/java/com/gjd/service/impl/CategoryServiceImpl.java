package com.gjd.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gjd.constants.SystemConstants;
import com.gjd.domain.ResponseResult;
import com.gjd.domain.entity.Category;
import com.gjd.domain.entity.LoginAdmin;
import com.gjd.domain.vo.CategoryVo;
import com.gjd.domain.vo.PageVo;
import com.gjd.domain.vo.enums.AppHttpCodeEnum;
import com.gjd.mapper.CategoryMapper;
import com.gjd.service.CategoryService;
import com.gjd.utils.BeanCopyUtils;
import com.gjd.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 分类表(Category)表服务实现类
 *
 * @author makejava
 * @since 2023-01-10 21:19:05
 */
@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public ResponseResult getCategoryList() {
        //查询状态正常的分类
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Category::getStatus, SystemConstants.CATEGORY_STATUS_NORMAL);
        List<Category> categoryList = categoryMapper.selectList(queryWrapper);
        List<CategoryVo> categoryVo = BeanCopyUtils.copyBeanList(categoryList, CategoryVo.class);
        return ResponseResult.okResult(categoryVo);
    }

    @Override
    public ResponseResult getCategoryListPage(Long pageNum, Long pageSize) {
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Category::getStatus, SystemConstants.CATEGORY_STATUS_NORMAL);
        Page<Category> page = new Page<>(pageNum, pageSize);
        page(page, queryWrapper);
        List<CategoryVo> categoryVoList = BeanCopyUtils.copyBeanList(page.getRecords(), CategoryVo.class);
        return ResponseResult.okResult(new PageVo(categoryVoList, page.getTotal()));
    }

    @Override
    public ResponseResult addCategory(Category category) {
        categoryMapper.insert(category);
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult updateCategory(Category category) {
        categoryMapper.updateById(category);
        return ResponseResult.okResult();
    }

    @Override
    public ResponseResult deleteCategory(Long categoryId) {
        if (categoryId <= 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.CATEGORYID_ERROR);
        }
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Category::getId, categoryId);
        categoryMapper.delete(queryWrapper);
        return ResponseResult.okResult();
    }

}
