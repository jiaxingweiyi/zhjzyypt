package com.gjd.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManageEmployerDto {

    private Long id;
    private String userName;
    private String nickName;
    private String sex;
    private String area;
    private int age;
    private String phoneNumber;
    private Integer points;
    private String vip;
    private Date createTime;

}
