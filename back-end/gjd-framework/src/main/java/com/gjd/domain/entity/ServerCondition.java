package com.gjd.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 所发放的家政人员信息表
 *
 * @TableName server_condition
 */
@TableName(value = "server_condition")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServerCondition implements Serializable {
    /**
     * 与该家政人员的id一致
     */
    // TODO: 是否有必要保留？
    @TableId
    private Integer id;

    /**
     * 家政人员id
     */
    private Integer serverId;

    /**
     * 类别：0为短期工，1为长期工
     */
    private Integer type;

    /**
     * 分类id
     */
    private Integer categoryId;

    /**
     * 手机号
     */
    private Long phone;

    /**
     * 待遇
     */
    private String treatment;

    /**
     * 上岗日期
     */
    private Date serverTime;

    /**
     * 服务范围
     */
    private String serverArea;

    /**
     * 薪资
     */
    private Double salary;

    /**
     * 默认公开 0为隐私，1为公开
     */
    private Integer status;

    /**
     * 是否住家
     */
    private Integer stay;

    private String serverType;

    private String area;

    private String workDate;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

}