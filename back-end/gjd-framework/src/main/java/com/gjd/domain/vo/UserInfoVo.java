package com.gjd.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoVo {
    /**
     * 主键
     */
    private Long id;

    //用户名
    private String userName;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 头像
     */
    private String avatar;

    private String sex;

    private String email;

    private String userType;

    private String serverType;

    private int age;

    private String area;

    private String workRange;

    private String skill;

    private String workArea;

    private String characteristic;

    private String cert;

    private String workExp;

    private String jobNum;

    private String phoneNumber;

    private Integer points;

    private String vip;

    private Integer salary;

    private String stay;

    private String categoryName;

    private Date createTime;
}