package com.gjd.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 返回验证码实体
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CodeObject implements Serializable {
    /**
     * 验证码
     */
    private String code;
    /**
     * 发送code的方式
     */
    private String object;
    /**
     * 身份标识
     */
    private String userType;
}
