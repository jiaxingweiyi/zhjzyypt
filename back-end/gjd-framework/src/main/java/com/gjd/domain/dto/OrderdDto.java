package com.gjd.domain.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gjd.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user")
public class OrderdDto extends User {

    private String nickName;
    private Integer delFlag;
    private String userType;
    private String workArea;
    private Integer salary;
    private Integer salaryOrder;
    private Long categoryId;
    private String categoryName;
    private Integer stay;
    private Double totalPrice;
    private String serverType;
    private Date serverTime;
    private Long serverId;
}
