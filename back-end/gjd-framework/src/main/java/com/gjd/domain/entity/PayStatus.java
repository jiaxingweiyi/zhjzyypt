package com.gjd.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PayStatus {

    private String outTradeNo;
    private String tradeNo;
    private String tradeStatus;
    private String appId;
    private String totalPrice;
}
