package com.gjd.domain.vo.enums;

public enum AppHttpCodeEnum {

    // TODO: 2022/11/5 响应的枚举类

    // 成功
    SUCCESS(200, "操作成功"),
    // 登录
    NEED_LOGIN(401, "token已过期，需要登录后操作"),
    NO_OPERATOR_AUTH(403, "无权限操作"),
    SYSTEM_ERROR(500, "出现错误"),
    USERNAME_EXIST(501, "用户名已存在"),
    NEED_REGISTER(402,"该账号未注册"),
    CODE_FAIL(502,"验证码错误或已失效"),
    CODE_NULL(506,"验证码不能为空"),
    CODE_SEND_FAIL(502,"验证码发送错误"),
    PHONENUMBER_EXIST(502, "手机号已存在"),
    EMAIL_EXIST(503, "邮箱已存在"),
    REQUIRE_USERNAME(504, "必需填写用户名"),
    LOGIN_ERROR(505, "用户名或密码错误"),
    CONTENT_NOT_NULL(506, "评论内容不能为空"),
    FILE_TYPE_ERROR(507, "文件类型错误，请上传png或jpg文件"),
    USERNAME_NOT_NULL(508, "用户名不能为空"),
    NICKNAME_NOT_NULL(509, "昵称不能为空"),
    PASSWORD_NOT_NULL(510, "密码不能为空"),
    PASSWORD_ERROR(514, "旧密码输入错误"),
    EMAIL_NOT_NULL(511, "邮箱不能为空"),
    NICKNAME_EXIST(512, "昵称已存在"),
    USERTYPE_NOT_NULL(513, "用户类型不能为空"),
    CATEGORYID_ERROR(514, "分类id不存在"),

    USERNAME_NO_EXIST(516, "用户不存在"),
    ORDER_ERRROR(620, "该订单不存在"),

    QRCODE_ERROR(610, "二维码生成错误");
    int code;
    String msg;

    AppHttpCodeEnum(int code, String errorMessage) {
        this.code = code;
        this.msg = errorMessage;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
