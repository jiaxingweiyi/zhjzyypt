package com.gjd.domain.vo;

import com.gjd.domain.entity.PayRecord;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PayRecordVo extends PayRecord {
    //二维码
    private String qrcode;
}
