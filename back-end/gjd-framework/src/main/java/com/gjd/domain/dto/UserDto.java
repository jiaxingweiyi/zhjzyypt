package com.gjd.domain.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gjd.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user")
public class UserDto extends User {

    private String nickName;
    private Integer delFlag;
    private String userType;
    private String serverType;
    private String workArea;
    private Integer salary;
    private Integer salaryOrder;
    private Long categoryId;
    private String categoryName;
    private Integer stay;
    private String status;
}
