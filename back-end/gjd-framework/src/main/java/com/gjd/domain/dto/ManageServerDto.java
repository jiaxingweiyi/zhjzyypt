package com.gjd.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManageServerDto {

    private Long id;
    private String userName;
    private String nickName;
    private String sex;
    private String area;
    private int age;
    private String phoneNumber;
    private Date createTime;

    private String status;
    private String workRange;
    private String skill;
    private String cert;
    private String characteristic;
    private String workExp;
}
