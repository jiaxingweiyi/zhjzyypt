package com.gjd.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 订单表
 * @TableName orders
 */
@TableName(value ="orders")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Orders implements Serializable {
    /**
     * 订单号
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 雇主id
     */
    private Long userId;

    /**
     * 家政人员id
     */
    private Long serverId;

    /**
     * 订单状态
     */
    private String status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 服务时间
     */
    private Date serverTime;

    /**
     * 总价
     */
    private Double totalPrice;

    /**
     * 订单描述
     */
    private String orderName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}