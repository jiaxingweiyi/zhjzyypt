package com.gjd.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderInfo {

//    order
    private Long Id;
    private Date createTime;
    private String status;
    private Date serverTime;
    private Double totalPrice;
    private Long userId;
    private Long serverId;

//    server_condition
    private String serverType;
    private String treatment;
    private String serverArea;

//    user
    private String categoryName;
    private String userName;
    private String serverName;
    private Long userPhone;
    private Long serverPhone;
    private String area;
}
