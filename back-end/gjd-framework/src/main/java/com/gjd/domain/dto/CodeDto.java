package com.gjd.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 验证码DTO
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CodeDto implements Serializable {
    private String code;

    private Object object;
}
