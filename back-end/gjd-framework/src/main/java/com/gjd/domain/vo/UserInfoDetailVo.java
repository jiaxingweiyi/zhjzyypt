package com.gjd.domain.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class UserInfoDetailVo {
    /**
     * 主键
     */
    private Long id;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 头像
     */
    private String avatar;

    private String sex;

    private String email;

    private String userType;

    private int age;

    private String area;

    private String workRange;

    private String skill;

    private String workArea;

    private String characteristic;

    private String cert;

    private String workExp;

    private String jobNum;

    private String phoneNumber;

    private Integer points;

    private String vip;

    private Integer salary;

    private String stay;

    private String categoryName;

    private String treatment;

    private Date serverTime;

    private String serverType;
}