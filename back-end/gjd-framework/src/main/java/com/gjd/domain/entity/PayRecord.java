package com.gjd.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;

/**
 * 支付记录表
 * @TableName pay_record
 */
@TableName(value ="pay_record")
@Data
public class PayRecord implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 订单号
     */
    private Long orderId;

    /**
     * 支付记录交易号
     */
    private Long payNo;

    /**
     * 家政人员id
     */
    private Long serverId;

    /**
     * 雇主id
     */
    private Long userId;

    /**
     * 
     */
    private String orderName;

    /**
     * 支付情况
     */
    private Integer status;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 
     */
    private Date serveTime;

    /**
     * 支付时间
     */
    private LocalDateTime paySucessTime;

    /**
     * 总金额
     */
    private Double totalPrice;

    /**
     * 第三方支付交易流水号
     */
    private String outPayNo;

    /**
     * 第三方交付渠道号
     */
    private Integer outPayChannel;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PayRecord other = (PayRecord) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getOrderId() == null ? other.getOrderId() == null : this.getOrderId().equals(other.getOrderId()))
            && (this.getPayNo() == null ? other.getPayNo() == null : this.getPayNo().equals(other.getPayNo()))
            && (this.getServerId() == null ? other.getServerId() == null : this.getServerId().equals(other.getServerId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getOrderName() == null ? other.getOrderName() == null : this.getOrderName().equals(other.getOrderName()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getServeTime() == null ? other.getServeTime() == null : this.getServeTime().equals(other.getServeTime()))
            && (this.getPaySucessTime() == null ? other.getPaySucessTime() == null : this.getPaySucessTime().equals(other.getPaySucessTime()))
            && (this.getTotalPrice() == null ? other.getTotalPrice() == null : this.getTotalPrice().equals(other.getTotalPrice()))
            && (this.getOutPayNo() == null ? other.getOutPayNo() == null : this.getOutPayNo().equals(other.getOutPayNo()))
            && (this.getOutPayChannel() == null ? other.getOutPayChannel() == null : this.getOutPayChannel().equals(other.getOutPayChannel()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
        result = prime * result + ((getPayNo() == null) ? 0 : getPayNo().hashCode());
        result = prime * result + ((getServerId() == null) ? 0 : getServerId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getOrderName() == null) ? 0 : getOrderName().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getServeTime() == null) ? 0 : getServeTime().hashCode());
        result = prime * result + ((getPaySucessTime() == null) ? 0 : getPaySucessTime().hashCode());
        result = prime * result + ((getTotalPrice() == null) ? 0 : getTotalPrice().hashCode());
        result = prime * result + ((getOutPayNo() == null) ? 0 : getOutPayNo().hashCode());
        result = prime * result + ((getOutPayChannel() == null) ? 0 : getOutPayChannel().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderId=").append(orderId);
        sb.append(", payNo=").append(payNo);
        sb.append(", serverId=").append(serverId);
        sb.append(", userId=").append(userId);
        sb.append(", orderName=").append(orderName);
        sb.append(", status=").append(status);
        sb.append(", createTime=").append(createTime);
        sb.append(", serveTime=").append(serveTime);
        sb.append(", paySucessTime=").append(paySucessTime);
        sb.append(", totalPrice=").append(totalPrice);
        sb.append(", outPayNo=").append(outPayNo);
        sb.append(", outPayChannel=").append(outPayChannel);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}