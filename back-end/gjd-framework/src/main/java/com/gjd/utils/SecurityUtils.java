package com.gjd.utils;

import com.gjd.domain.entity.LoginAdmin;
import com.gjd.domain.entity.LoginUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class SecurityUtils
{

    /**
     * TODO：获取当前用户信息
     **/
    public static LoginUser getLoginUser()
    {
        return (LoginUser) getAuthentication().getPrincipal();
    }

    public static LoginAdmin getLoginAdmin() {
        return (LoginAdmin) getAuthentication().getPrincipal();
    }
    /**
     * 获取Authentication
     */
    public static Authentication getAuthentication() {
        //从SecurityContextHolder获取认证信息
        return SecurityContextHolder.getContext().getAuthentication();
    }


    /**
     * 生成BCryptPasswordEncoder密码
     * @param password 密码
     * @return 加密字符串
     */
    public static String encodePassword(String password)
    {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    public static Long getUserId() {
        //获取登录对象信息--获取对象信息--获取对象id
        return getLoginUser().getUser().getId();
    }

    public static Long getAdminId(){
        return getLoginAdmin().getAdmin().getId();
    }
}