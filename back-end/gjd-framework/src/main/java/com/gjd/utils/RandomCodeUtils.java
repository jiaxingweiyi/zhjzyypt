package com.gjd.utils;

public class RandomCodeUtils {
    /**
     * 生成6位纯数字的验证码
     * @return
     */
    public static String getRandomCode(){
        long rand = (long)(Math.random()*900000+100000);
        return String.valueOf(rand);
    }
}
