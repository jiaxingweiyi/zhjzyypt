package com.gjd.controller;

import com.gjd.domain.ResponseResult;
import com.gjd.domain.entity.Admin;
import com.gjd.domain.entity.CodeObject;
import com.gjd.domain.entity.User;
import com.gjd.domain.vo.enums.AppHttpCodeEnum;
import com.gjd.service.AdminLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
@RequestMapping("/admin")
public class LoginController {

    @Autowired
    private AdminLoginService adminLoginService;

    @PostMapping("/login")
    public ResponseResult login(@RequestBody Admin admin){
        return adminLoginService.login(admin);
    }
    @PostMapping("/loginByEmail")
    public ResponseResult loginByEmail(@RequestBody Admin admin){
        return adminLoginService.loginByEmail(admin);
    }

    @PostMapping("/handleCode")
    public ResponseResult handleCode(@RequestBody CodeObject object){
        return adminLoginService.handleCode(object);
    }
    @PostMapping("/logout")
    public ResponseResult logout(){
        return adminLoginService.logout();
    }

    @PostMapping("/loginByTelephone")
    ResponseResult loginByCode(@RequestBody Admin admin){
        if (Objects.isNull(admin)){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        return adminLoginService.loginByPhoneAuthCode(admin);
    }
}
