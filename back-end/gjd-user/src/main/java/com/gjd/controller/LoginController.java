package com.gjd.controller;

import com.gjd.domain.ResponseResult;
import com.gjd.domain.entity.CodeObject;
import com.gjd.domain.entity.User;
import com.gjd.domain.vo.enums.AppHttpCodeEnum;
import com.gjd.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
@RequestMapping("/user")
public class LoginController {

    @Autowired
    private UserLoginService userLoginService;

    @PostMapping("/login")
    public ResponseResult login(@RequestBody User user){
        return userLoginService.login(user);
    }

    @PostMapping("/loginByEmail")
    public ResponseResult loginByEmail(@RequestBody User user){
        return userLoginService.loginByEmail(user);
    }

    @PostMapping("/loginByTelephone")
    ResponseResult loginByCode(@RequestBody User userInfo){
        if (Objects.isNull(userInfo)){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        return userLoginService.loginByPhoneAuthCode(userInfo);
    }

    @PostMapping("/handleCode")
    public ResponseResult handleCode(@RequestBody CodeObject object){
        return userLoginService.handleCode(object);
    }

    @PostMapping("/logout")
    public ResponseResult logout() {
        return userLoginService.logout();
    }
}
