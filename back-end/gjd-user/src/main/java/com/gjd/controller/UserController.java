package com.gjd.controller;

import com.gjd.domain.ResponseResult;
import com.gjd.domain.entity.Passwords;
import com.gjd.domain.entity.User;
import com.gjd.service.UserService;
import org.apache.xmlbeans.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public ResponseResult register(@RequestBody User user) {
        return userService.register(user);
    }

    @GetMapping("/serverList")
    public ResponseResult serverList(Long categoryId, Integer pageNum, Integer pageSize) {
        return userService.getServerList(categoryId,pageNum,pageSize);
    }

    @GetMapping("/userInfo")
    public ResponseResult userInfo(){
        return userService.getuserInfo();
    }

    @GetMapping("/cheackUserInfo")
    public ResponseResult cheackUserInfo(Long serverId){
        return userService.cheackUserInfo(serverId);
    }

    @PostMapping("/upload")
    public ResponseResult uploadImg(MultipartFile img) {
        return userService.uploadImg(img);
    }

    @PutMapping("/userInfo")
    public ResponseResult updateUserInfo(@RequestBody User user){
        return userService.updateUserInfo(user);
    }

    @PostMapping("/userPassword")
    public ResponseResult updatePassword(@RequestBody Passwords passwords){
        return userService.updatePassword(passwords);
    }

    @PostMapping("/checkToken")
    public ResponseResult checkToken(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        return userService.checkToken(request,response);
    }

}
