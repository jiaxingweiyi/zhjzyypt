package com.gjd.controller;

import com.gjd.domain.ResponseResult;
import com.gjd.domain.entity.ServerCondition;
import com.gjd.service.ServerConditionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/condition")
public class ConditionController {

    @Autowired
    private ServerConditionService serverConditionService;

    @PostMapping("/refineCondition")
    public ResponseResult refineCondition(@RequestBody ServerCondition serverCondition) {
        return serverConditionService.refineCondition(serverCondition);
    }

    @GetMapping("/getCondition")
    public ResponseResult getCondition(Long serverId) {
        return serverConditionService.getCondition(serverId);
    }
}
