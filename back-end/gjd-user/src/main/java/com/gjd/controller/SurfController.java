package com.gjd.controller;

import com.gjd.domain.ResponseResult;
import com.gjd.domain.dto.UserDto;
import com.gjd.domain.entity.User;
import com.gjd.service.UserSurfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/user")
public class SurfController {

    @Autowired
    private UserSurfService userSurfService;

    @GetMapping("/surf")
    public ResponseResult surfAndResult(@RequestParam("field") String field, @RequestParam("page") Long page, @RequestHeader String token) {
        return userSurfService.surf(field, token, page, 5L);
    }

    @GetMapping("/selectUserByConditonPage")
    public ResponseResult selectUserByConditonPage(UserDto userDto, Long pageNum, Long pageSize) {
        return userSurfService.selectUserByConditonPage(userDto,pageNum,pageSize);
    }
}
