package com.gjd.controller;

import com.gjd.domain.ResponseResult;
import com.gjd.domain.dto.OrderdDto;
import com.gjd.domain.vo.OrderInfo;
import com.gjd.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user/order")
public class OrderController {

    @Autowired
    private OrdersService ordersService;

    @PostMapping("/createOrder")
    public ResponseResult createOrder(OrderInfo orderInfo) {
        return ordersService.createOrder(orderInfo);
    }

    @PostMapping("/createorder")
    public ResponseResult createOrder(OrderdDto orderdDto) {
        return ordersService.createOrder(orderdDto);
    }

    @GetMapping("/getUserOrder")
    public ResponseResult getUserOrder(String orderStatus, Long pageNum, Long pageSize) {
        return ordersService.getUserOrder(orderStatus, pageNum, pageSize);
    }

    @GetMapping("/getServerOrder")
    public ResponseResult getServerOrder(String orderStatus, Long pageNum, Long pageSize){
        return ordersService.getServerOrder(orderStatus,pageNum,pageSize);
    }

    @PostMapping("/cancelOrder")
    public ResponseResult cancelOrder(Long orderId) {
        return ordersService.cancelOrder(orderId);
    }
}
