package com.gjd.controller;

import com.gjd.domain.ResponseResult;
import com.gjd.domain.entity.Comment;
import com.gjd.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @GetMapping("/commentList")
    public ResponseResult commentList(Long serverId, Integer pageNum, Integer pageSize){
        return commentService.commentList(serverId,pageNum,pageSize);
    }

    @PostMapping("/addComment")
    public ResponseResult addComment(@RequestBody Comment comment){
        return commentService.addComment(comment);
    }

    @GetMapping("/myComment")
    public ResponseResult myComment(Long serverId,Long pageNum,Long pageSize){
        return commentService.myComment(serverId,pageNum,pageSize);
    }
}
