import serverConditionRequest  from '@/utils/serverConditionRequest'
// 按条件查询
export function getListByCondition(params) {
    return serverConditionRequest({
        url: '/admin/serverCondition/selectServerConditionByCondition',
        method: 'get',
        params: params
    })
}

// 通过Id查询发布状态
export function getById(param) {
    return serverConditionRequest({
        url: '/admin/serverCondition/selectServerConditionById',
        method: 'get',
        params: {
            serverId: param
        }
    })
}

// 审核通过
export function agree(param) {
    return serverConditionRequest({
        url: '/admin/serverCondition/agreeCondition',
        method: 'post',
        params: {
            serverId: param
        }
    })
}

// 审核不通过
export function disagree(param) {
    return serverConditionRequest({
        url: '/admin/serverCondition/disagreeCondition',
        method: 'post',
        params: param
    })
}