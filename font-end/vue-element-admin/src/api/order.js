import adminRequest from '@/utils/adminRequest'

// 根据条件获取信息
export function GetOrdersList(parmas) {
  return adminRequest({
    url: '/admin/orders/selectOrdersListByCondition',
    method: 'get',
    params: parmas
  })
}

// 根据id获取订单
export function GetOrderById(id) {
  return adminRequest({
    url: '/admin/orders/selectOrderById',
    method: 'get',
    params: {
      orderId: id
    }
  })
}

// 根据id删除订单
export function deleteOrder(parmas) {
  return adminRequest({
    url: '/admin/orders/deleteOrders',
    method: 'get',
    parmas: parmas
  })
}

// 根据id查询订单日志
export function getLogById(parmas) {
  return adminRequest({
    url: '/admin/orders/selectOrderLogById',
    method: 'get',
    params: parmas
  })
}

// 修改订单信息
export function updateOrder(parmas) {
  return adminRequest({
    url: '/admin/orders/updateOrder',
    method: 'put',
    params: parmas
  })
}