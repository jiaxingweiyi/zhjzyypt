import adminRequest from '@/utils/adminRequest'

// 获取家政分类数据
export function getCategoryList() {
  return adminRequest({
    url: '/admin/category/categoryListPage?pageNum=1&pageSize=100',
    method: 'get'
  })
}

// 条件查询分类
export function getCategoryListByName(param) {
  return adminRequest({
    url: "/admin/category/getCategoryByName?pageNum=1&pageSize=100",
    method: "get",
    params: param
  });
}

// 修改家政分类数据
export function updateCategoryList(parmas) {
  return adminRequest({
    url: '/admin/category/updateCategory',
    method: 'put',
    data: parmas
  })
}

// 添加家政分类数据
export function addCategoryList(parmas) {
  return adminRequest({
    url: '/admin/category/addCategory',
    method: 'post',
    data: parmas
  })
}

// 删除家政分类数据
export function delCategoryList(p) {
  return adminRequest({
    url: '/admin/category/deleteCategory',
    method: 'post',
    params: {
      categoryId: p
    }
  })
}

// 查询用户
export function userList(p) {
  return adminRequest({
    url: '/admin/manage/userListPage',
    method: 'get',
    params: p
  })
}
// 修改用户
export function updateUserInfo(parmas) {
  return adminRequest({
    url: '/admin/manage/updateUserInfo',
    method: 'put',
    data: parmas
  })
}

// 删除用户
export function delUserInfo(p) {
  return adminRequest({
    url: '/admin/manage/deleteUser?userId=' + p,
    method: 'delete'
  })
}

// 条件查询用户
export function selectUserInfo(p) {
  return adminRequest({
    url: '/admin/manage/selectUserListPage',
    method: 'get',
    params: p
  })
}
// 条件查询用户
export function exportCategoryExcel(p) {
  return adminRequest({
    url: '/admin/category/exportCategory',
    method: 'get',
    params: p,
    responseType: "blob"
  })
}

// 登录
export function login(p) {
  return adminRequest({
    url: '/admin/login',
    method: 'post',
    data: p,
  })
}

// 退出登录
export function logout() {
  return adminRequest({
    url: '/admin/logout',
    method: 'post',
  })
}