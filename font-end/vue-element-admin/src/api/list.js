import serverRequest from "@/utils/serverRequest";

export function getCategoryList() {
  return serverRequest({
    url: "/category/categoryList", // 假地址 自行替换
    method: "get",
  });
}
