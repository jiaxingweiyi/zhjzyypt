import Cookies from 'js-cookie'

const TokenKey = 'token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function setAvatar(avatar) {
  return Cookies.set('avatar', avatar)
}

export function getAvatar() {
  return Cookies.get('avatar')
}

export function setName(name) {
  return Cookies.set('adminName', name)
}

export function getName() {
  return Cookies.get('adminName')
}

export function removeName() {
  return Cookies.remove('adminName')
}

export function setId(id) {
  return Cookies.set('id', id)
}

export function getId() {
  return Cookies.get('id')
}

export function removeId() {
  return Cookies.remove('id')
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function removeAvatar() {
  return Cookies.remove('avatar')
}
