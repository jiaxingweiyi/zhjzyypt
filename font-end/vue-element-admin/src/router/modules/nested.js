/** When your routing table is too long, you can split it into small modules **/

import Layout from "@/layout";

const nestedRouter = {
  path: "/nested",
  component: Layout,
  redirect: "/nested/menu1/menu1-1",
  name: "Nested",
  meta: {
    title: "nested",
    icon: "nested",
  },
  children: [
    {
      path: "menu1",
      name: "Menu1",
      component: () => import("@/views/userManagement/bossManagement/index"),
      meta: { title: "menu1" },
    },
    {
      path: "menu2",
      name: "Menu2",
      component: () => import("@/views/userManagement/serverManagement/index"),
      meta: { title: "menu2" },
    },
  ],
};

export default nestedRouter;
