const state = {
  currentPage: 1,
  currentPageSize: 3,
  parmas: {
    pageNum: 1,
    pageSize: 3,
    nickName: "",
    salaryOrder: 0,
    categoryName: "不限",
    salary: 0,
    stay: 0,
    userType: 0,
    workArea: "",
    introdction: '',
  },
  searchList: null,
  total: 0,
  id: null,
  isByCondition: false
};
const mutations = {
  setList(state, List) {
    state.searchList = List;
  },
  changePageNum(state, val) {
    state.parmas.pageNum = val;
    state.currentPage = val;
  },
  changePageSize(state, val) {
    state.parmas.pageSize = val;
    state.currentPageSize = val;
  },
  setId(state, i) {
    state.id = i;
  },
  setSalary(state, p) {
    state.parmas.salary = p;
  },
  setParmas(state, p) {
    state.parmas = p;
  },
  setTotal(state, t) {
    state.total = t;
  },
  changeIsByCondition(state,t){
    state.isByCondition = t
  }
};

const actions = {
};
const getters = {
  getSearchList(state) {
    return state.searchList;
  },
  getPageNum(state) {
    return state.currentPage;
  },
  getPageSize(state) {
    return state.currentPageSize;
  },
  getParmas(state) {
    return state.parmas;
  },
  getTotal(state) {
    return state.total;
  },
  getId(state) {
    return state.id;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
