const state = {
  //个人中心的路由导航
  showBasic: true,
  showExperience: false,
  showPwd: false,
  showComment: false,
  showStatus: false,
  showOrder: false,
  serverId: "",
  orderTotal: null,
  orderList: [],
  defaultIndex: "1-1",
};
const mutations = {
  //订单总数
  setOrderTotal(state, t) {
    state.orderTotal = t;
  },
  //设置订单信息
  setOrderList(state, l) {
    state.orderList = l;
  },
  //改变路由的方法
  convertIndex(state, index) {
    state.defaultIndex = index
    if (index == "1-1") {
      state.showBasic = true;
      state.showExperience = false;
      state.showPwd = false;
      state.showComment = false;
      state.showStatus = false;
      state.showOrder = false;
    } else if (index == "1-2") {
      state.showBasic = false;
      state.showExperience = true;
      state.showPwd = false;
      state.showComment = false;
      state.showStatus = false;
      state.showOrder = false;
    } else if (index == "0-1") {
      state.showBasic = false;
      state.showExperience = false;
      state.showPwd = true;
      state.showComment = false;
      state.showStatus = false;
      state.showOrder = false;
    } else if (index == "2") {
      state.showOrder = false;
      state.showBasic = false;
      state.showExperience = false;
      state.showPwd = false;
      state.showComment = false;
      state.showStatus = true;
    } else if (index == "4") {
      state.showOrder = false;
      state.showBasic = false;
      state.showExperience = false;
      state.showPwd = false;
      state.showComment = true;
      state.showStatus = false;
    } else if (index == "3") {
      state.showBasic = false;
      state.showExperience = false;
      state.showPwd = false;
      state.showComment = false;
      state.showStatus = false;
      state.showOrder = true;
    }
  },
};
const actions = {
  /*   async postComment({ commit }, { parmas, t }) {
    let result = await Comment(params, t);
    if (result.data.code) {
      return "ok";
    } else {
      return Promise.reject(new Error("faile"));
    }
  }, */
};
const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
