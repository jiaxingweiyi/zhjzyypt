const state = {
  login_name: "",
  tokenStore: "",
};
const mutations = {
  setLoginName(state, name) {
    state.login_name = name;
  },
};
const actions = {};
const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
