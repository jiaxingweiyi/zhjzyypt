import { createStore } from "vuex";
import LoginStore from "./LoginStore";
import PerCent from "./PerCent";
import SearchStore from "./SearchStore";
import OrderStore from "./OrderStore";
export default createStore({
  state: {
    userTypeStore: "",
  },
  mutations: {
    changeType(state, p) {
      state.userTypeStore = p;
    },
  },
  modules: {
    LoginStore: LoginStore,
    PerCent: PerCent,
    SearchStore: SearchStore,
    OrderStore: OrderStore,
  },
});
