const state = {
  orderStatus: '全部',
  pageNum: 1,
  pageSize: 2,
  total: 0,
};
const mutations = {
  setStatus(state, s) {
    state.orderStatus = s;
  },
  changePageNum(state, n) {
    state.pageNum = n;
  },
  setTotal(state, t) {
    state.total = t;
  },
  changeCurrentPageSize(state, t) {
    state.pageSize = t;
  },
};
const getters = {
  getTotal(state) {
    return state.total;
  },
  getStatus(state) {
    return state.orderStatus;
  },
  getCurrentPage(state) {
    return state.pageNum;
  },
  getCurrentPageSize(state) {
    return state.pageSize;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  getters,
};
