import axios from "axios";
import router from "@/router";
import "nprogress/nprogress.css";
import popmessage from "@/components/tip/index.js";

const requests = axios.create({
  baseURL: "http://81.71.3.215:7777",
  timeout: 25000,
});

requests.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    var token = localStorage.getItem("token");

    if (token == "" || token == null || token == "undefined") {
    } else {
      config.headers.token = localStorage.getItem("token");
    }
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器 => 后端给前端的数据【后端返回给前端的东西】
requests.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    if (response.data.code == 401) {
      localStorage.removeItem("token");
      popmessage({ type: "error", str: "登陆已过期，请重新登录" });
      router.push("/loginView");
      setTimeout(() => {
        router.go(0);
      }, 1000);
    }
    return response;
  },
  function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
  }
);

// requests.interceptors.request.use((config) => {
//   //进度条开始动
//   nprogress.start();
//   if (localStorage.getItem("token")) {
//     // config.headers.token = localStorage.getItem("token");
//   }
//   return config;
// });
// //响应拦截器
// requests.interceptors.response.use(
//   (res) => {
//     //进度条结束
//     nprogress.done();
//     return res.data;
//   },
//   (err) => {
//     return Promise.reject(new Error("faile"));
//   }
// );

export default requests;
