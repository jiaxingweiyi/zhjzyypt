//api接口统一管理

import request from "./request";

//用户注册
export const UserRegis = (params) =>
  request({
    url: "/user/register",
    method: "post",
    data: params,
  });
//用户登录
export const UserLogin = (params) =>
  request({
    url: "/user/login",
    method: "post",
    data: params,
  });
//用户退出登录
export const Logout = () =>
  request({
    url: "/user/logout",
    method: "post",
  });
//发表评论
export const Comment = (params) =>
  request({
    url: "/comment/addComment",
    method: "post",
    data: params,
  });
// 对家政订单进行模糊查询
export const searchByIntroduction = (params) =>
  request({
    url: "/user/selectServerByIntroduction",
    method: "get",
    params: params,
  });
  //家政人员显示评论
export const serverCommentList = (p) =>
request({
  url: "/comment/serverCommentList",
  method: "get",
  params: p,
});
//显示雇主评论
export const BossComment = (p) =>
  request({
    url: "/comment/commentList",
    method: "get",
    params: p,
  });

//显示家政信息
export const ServerList = () =>
  request({
    url: "/user/serverList",
    method: "get",
  });
//显示个人信息
export const userInfo = () =>
  request({
    url: "/user/userInfo",
    method: "get",
  });
//更新个人信息
export const convertInfo = (params) =>
  request({
    url: "/user/userInfo",
    method: "put",
    data: params,
  });
//上传头像
export const avatarUp = (params) =>
  request({
    url: "/user/upload",
    method: "post",
    data: params,
  });
//修改密码
export const changePwd = (parmas) =>
  request({
    url: "	user/userPassword",
    method: "post",
    data: parmas,
  });
//发布状态
export const postStatus = (params) =>
  request({
    url: "/condition/refineCondition",
    method: "post",
    data: params,
  });
//搜索家政人员
export const searchServer = (p) =>
  request({
    url: "/user/selectUserByConditonPage",
    method: "get",
    params: p,
  });
//无条件查询家政人员
export const searchServerWithNoCondition = (p) =>
  request({
    url: "/user/selectServerWithoutCondition",
    method: "get",
    params: p,
  });
//显示家政人员详细详细
export const serverDetailApi = (p) =>
  request({
    url: "/user/cheackUserInfo",
    method: "get",
    params: {
      serverId: p,
    },
  });
//显示搜索栏的家政分类

export const categoryListApi = () =>
  request({
    url: "/category/categoryList",
    method: "get",
  });
//雇主通过orderId查询响应订单
export const getBossOrderByOrderId = (p) =>
  request({
    url: "/comment/serverCommentListByOrderIdAndUserId",
    method: "get",
    params: p,
  });
//雇主显示订单
export const getBossOrder = (p) =>
  request({
    url: "/user/order/getUserOrder",
    method: "get",
    params: p,
  });
//显示家政人员订单
export const getServerOrder = (p) =>
  request({
    url: "/user/order/getServerOrder",
    method: "get",
    params: p,
  });

//提交订单
export const generatePayCode = (p) =>
  request({
    url: "/user/pay/generatePayCode",
    method: "post",
    data: p,
  });

//取消订单
export const cancelOrderById = (p) =>
  request({
    url: "/user/order/cancelOrder",
    method: "post",
    params: p,
  });
// 完成订单
export const finishOrder = (params) =>
  request({
    url: "/user/order/completeOrder",
    method: "post",
    params: params,
  });
//支付
export const requestPay = (p) =>
  request({
    url: "/user/pay/requestPay",
    method: "get",
    parmas: p,
  });
// 在我的订单页中去支付
export const toPay = (p) =>
  request({
    url: "/user/pay/getPayNoByOrderId",
    method: "post",
    params: p
  });
//手机号登陆获取验证码
export const phoneValidate = (p) =>
  request({
    url: "/user/loginByTelephone",
    method: "post",
    data: p,
  });
//手机号登陆
export const phoneLogin = (params) =>
  request({
    url: "/user/handleCode",
    method: "post",
    data: params,
  });

//邮箱获取验证码
export const emailValidate = (p) =>
  request({
    url: "/user/loginByEmail",
    method: "post",
    data: p,
  });
//邮箱登陆
export const emailLogin = (p) =>
  request({
    url: "/user/handleCode",
    method: "post",
    data: p,
  });

//检查token是否过期
export const checkToken = () =>
  request({
    url: "/user/checkToken",
    method: "post",
  });

//查看发布状态
export const getCondition = (p) =>
  request({
    url: "/condition/getCondition",
    method: "get",
    params: p,
  });
