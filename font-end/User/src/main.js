import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import axios from "axios";
import VueAxios from "vue-axios";
import "./util/rem";
import ElementPlus from "element-plus";
import "element-plus/theme-chalk/index.css";
import locale from "element-plus/lib/locale/lang/zh-cn";

import * as ElIcon from "@element-plus/icons-vue";
axios.defaults.baseURL = "http://81.71.3.215:7777";
const app = createApp(App);
app
  .use(store)
  .use(router)
  .use(VueAxios, axios)
  .use(ElementPlus, { locale })
  .mount("#app");

for (const [key, component] of Object.entries(ElIcon)) {
  app.component(key, component);
}
