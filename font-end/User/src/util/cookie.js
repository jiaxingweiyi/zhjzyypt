//cookie的设置
var setCookie = function (name, value, day) {
  var strCookie = "";
  //判断name和value是否存在
  if (name && value) {
    strCookie =
      encodeURIComponent(name) + "=" + encodeURIComponent(value) + ";";
  }
  //判断时间
  if (typeof day == "number") {
    var date = new Date();
    date.setDate(date.getDate() + day);
    strCookie += "expires=" + date.toUTCString() + ";";
  }

  return (document.cookie = strCookie);
};

//获取cookie
var getCookie = function (name) {
  var cookieStr = decodeURIComponent(document.cookie);
  var arr1 = cookieStr.split(";"); //[aa="xxx",bb="xxx"]
  for (var i = 0; i < arr1.length; i++) {
    var arr2 = arr1[i].split("=");
    if (arr2[0].trim() === name) {
      return arr2[1];
    }
  }
};

//删除cookie
var delCookie = function (name) {
  setCookie(name, 1, -1);
};

var deleteAllCookies =function () {
  var cookies = document.cookie.split(';');
  for (var i = 0; i < cookies.length; i++) {
    var cookie = cookies[i];
    var eqPos = cookie.indexOf('=');
    var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
    document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
  }
}

//导出
export { setCookie, getCookie, delCookie };
