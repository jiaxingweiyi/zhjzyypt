import { createRouter, createWebHashHistory } from "vue-router";
import TheHome from "../views/TheHome";
import UserLogin from "../views/UserLogin";
import PerCent from "../views/PreCent";
import UserReg from "../views/UserReg";
import Introduce from "../views/Introduce";
import BossHome from "../views/BossHome";
import BossPerCent from "../views/BossPerCent";
import Search from "../views/Search";
import ServeDetail from "../views/ServeDetail";
import ConfirmOrder from "../views/Search/ConfirmOrder";
import UserHelp from "../views/UserHelp";

const routes = [
  {
    path: "/",
    redirect: {
      name: "introduce",
    },
    meta: {
      isShow: true,
    },
  },
  {
    path: "/loginview",
    name: "loginview",
    component: UserLogin,
    meta: {
      isShow: true,
    },
  },
  {
    path: "/thehome",
    name: "thehome",
    component: TheHome,
    meta: {
      isShow: true,
    },
  },
  {
    path: "/percent",
    name: "percent",
    component: PerCent,
    meta: {
      isShow: true,
    },
  },
  {
    path: "/userreg",
    name: "userreg",
    component: UserReg,
    meta: {
      isShow: true,
    },
  },

  {
    path: "/bosshome/:username",
    name: "bosshome",
    component: BossHome,
    meta: {
      isShow: true,
    },
  },
  {
    path: "/introduce",
    name: "introduce",
    component: Introduce,
    meta: {
      isShow: true,
    },
  },
  {
    path: "/boss_per_cent",
    name: "boss_per_cent",
    component: BossPerCent,
    meta: {
      isShow: true,
    },
  },
  {
    path: "/search",
    name: "search",
    component: Search,
    meta: {
      isShow: true,
    },
  },
  {
    path: "/serve_detail",
    name: "serve_detail",
    component: ServeDetail,
    meta: {
      isShow: true,
    },
  },
  {
    path: "/confirm_order",
    name: "confirm_order",
    component: ConfirmOrder,
    meta: {
      isShow: true,
    },
  },
  {
    path: "/UserHelp",
    name: "UserHelp",
    component: UserHelp,
    meta: {
      isShow: true,
    },
  },

];

const router = createRouter({
  mode: "history",
  history: createWebHashHistory(),
  routes,
  base: "./liang",
});
router.beforeEach((to, from, next) => {
  let token = localStorage.getItem("token");

  if (token == "") {
    if (to.fullPath == "/boss_per_cent" || to.fullPath == "/percent") {
      alert("未登录不能进入此页面");
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
