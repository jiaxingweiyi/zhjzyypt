const { defineConfig } = require("@vue/cli-service");

// 引入等比适配插件
const px2rem = require("postcss-px2rem");

const webpack = require("webpack");
// 配置基本大小
const postcss = px2rem({
  // 基准大小 baseSize，需要和rem.js中相同
  // remUnit: 14 代表 1rem = 14px; 所以当你一个14px值时，它会自动转成 (14px/14)rem
  remUnit: 20,
});

module.exports = defineConfig({
  transpileDependencies: true,

  publicPath: "./",
  lintOnSave: false,
  chainWebpack: (config) => {
    config.plugin("provide").use(webpack.ProvidePlugin, [
      {
        $: "jquery",
        jquery: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery",
      },
    ]);
  },
});
module.exports = {
  pwa: {
      iconPaths: {
          favicon32: 'favicon.ico',
          favicon16: 'favicon.ico',
          appleTouchIcon: 'favicon.ico',
          maskIcon: 'favicon.ico',
          msTileImage: 'favicon.ico'
      }
  }
};
 