const state = {
  isLogin: false, //记录是否已经登录
};
const mutations = {
  setLogin(state) {
    state.isLogin = true;
  },
};
const actions = {};
const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
