import { createStore } from "vuex";
import LoginMessage from "./LoginMessage";
export default createStore({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    LoginMessage,
  },
});
