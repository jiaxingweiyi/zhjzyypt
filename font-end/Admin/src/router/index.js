import { createRouter, createWebHashHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import AdminLogin from "../views/AdminLogin";
import AdminHome from "../views/AdminHome";
import AdminReg from "../views/AdminReg";
import store from "../store";
const routes = [
  {
    path: "/",
    redirect: {
      name: "adminlogin",
    },
  },

  {
    path: "/adminlogin",
    name: "adminlogin",
    component: AdminLogin,
  },
  {
    path: "/adminhome",
    name: "adminhome",
    component: AdminHome,
  },
  {
    path: "/adminreg",
    name: "adminreg",
    component: AdminReg,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
  base: "/admin",
});

router.beforeEach((to, from, next) => {
  // to and from are both route objects. must call `next`.
  console.log("@");
  console.log(to.fullPath, from);
  if (to.fullPath == "/adminlogin") next();
  //检测是否登录
  else if (store.state.LoginMessage.isLogin) {
    next();
  }
});

export default router;
