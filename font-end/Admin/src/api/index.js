//api接口统一管理

import request from "./request";

export const UserLogin = (params) =>
  request({ url: "/user/login", method: "post", data: params });
