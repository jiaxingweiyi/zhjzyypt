create table order_log
(
    id         int auto_increment comment '主键编号'
        primary key,
    order_id   int         not null comment '订单号',
    order_item varchar(20) not null comment '订单字段名',
    log_time   datetime    not null comment '日志时间'
)
    comment '订单日志表';

INSERT INTO 家政预约.order_log (id, order_id, order_item, log_time) VALUES (1, 1, 'total_price', '2033-05-01 03:03:03');
INSERT INTO 家政预约.order_log (id, order_id, order_item, log_time) VALUES (2, 1, 'total_price', '2023-03-22 23:52:30');
INSERT INTO 家政预约.order_log (id, order_id, order_item, log_time) VALUES (3, 1, 'work_time', '2023-03-22 23:52:48');