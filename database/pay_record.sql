create table pay_record
(
    id              bigint auto_increment
        primary key,
    order_id        bigint      null comment '订单号',
    pay_no          bigint      null comment '支付记录交易号',
    server_id       bigint      null comment '家政人员id',
    user_id         bigint      null comment '雇主id',
    order_name      varchar(20) null,
    status          int         null comment '支付情况',
    create_time     datetime    null comment '创建时间',
    serve_time      datetime    null,
    pay_sucess_time datetime    null comment '支付时间',
    total_price     float(8, 2) null comment '总金额',
    out_pay_no      varchar(64) null comment '第三方支付交易流水号',
    out_pay_channel int         null comment '第三方交付渠道号'
)
    comment '支付记录表';

INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (1, null, 1, 1, 1, null, 601, '2023-01-16 17:13:03', null, null, 1, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (2, null, 2, 2, 2, null, 601, '2023-01-16 17:21:19', null, null, 2, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (3, null, 3, 2, 2, null, 601, '2023-01-16 17:23:50', null, null, 2, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (4, 6, 1614958213680521216, 3, 3, null, 601, '2023-01-16 20:10:33', null, null, 3, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (5, 7, 1614959170615173120, 4, 4, null, 601, '2023-01-16 20:14:05', null, null, 4, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (6, 8, 1614959239944216576, 4, 4, null, 601, '2023-01-16 20:14:22', null, null, 4, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (7, 9, 1614959998852399104, 4, 4, null, 601, '2023-01-16 20:17:23', null, null, 4, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (8, 10, 1614960284987817984, 4, 4, null, 601, '2023-01-16 20:18:31', null, null, 4, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (9, 11, 1614960399609479168, 4, 4, null, 601, '2023-01-16 20:18:58', null, null, 4, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (10, 13, 1614961233876328448, 4, 4, null, 601, '2023-01-16 20:22:17', '1970-01-01 08:00:00', null, 4, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (11, 14, 1614973266262659072, 4, 4, null, 601, '2023-01-16 21:10:06', '1970-01-01 08:00:00', null, 4, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (12, 15, 1615180094091984896, 4, 4, null, 601, '2023-01-17 10:51:58', '1970-01-01 08:00:00', null, 4, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (13, 16, 1615358737455517696, 4, 4, null, 601, '2023-01-17 22:41:50', '1970-01-01 08:00:00', null, 1000, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (14, 17, 1615597098297958400, 5, 4, null, 601, '2023-01-18 14:28:59', '1970-01-01 08:00:00', null, 1, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (15, 18, 1615597982841442304, 5, 4, null, 601, '2023-01-18 14:32:30', '1970-01-01 08:00:00', null, 1, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (16, 19, 1615598614897889280, 5, 4, null, 601, '2023-01-18 14:35:01', '1970-01-01 08:00:00', null, 1, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (17, 20, 1615599249137602560, 5, 4, null, 601, '2023-01-18 14:37:32', '1970-01-01 08:00:00', null, 1, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (18, 21, 1615599663398875136, 5, 4, null, 601, '2023-01-18 14:39:11', '1970-01-01 08:00:00', null, 1, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (19, 22, 1615607610094850048, 5, 4, null, 601, '2023-01-18 15:10:45', '1970-01-01 08:00:00', null, 1, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (20, 23, 1615608540647481344, 5, 4, null, 601, '2023-01-18 15:14:27', '1970-01-01 08:00:00', null, 1, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (21, 24, 1615611769581125632, 5, 4, null, 601, '2023-01-18 15:27:17', '1970-01-01 08:00:00', null, 1, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (22, 25, 1619242408479911936, 5, 4, null, 601, '2023-01-28 15:54:09', '1970-01-01 08:00:00', null, 1, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (23, 26, 1619243308482441216, 5, 4, null, 601, '2023-01-28 15:57:43', '1970-01-01 08:00:00', null, 1, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (24, 27, 1620796873450930176, 5, 4, null, 601, '2023-02-01 22:51:02', '1970-01-01 08:00:00', null, 1, null, null);
INSERT INTO 家政预约.pay_record (id, order_id, pay_no, server_id, user_id, order_name, status, create_time, serve_time, pay_sucess_time, total_price, out_pay_no, out_pay_channel) VALUES (25, 29, 1622867733172797440, 5, 4, null, 601, '2023-02-07 15:59:54', null, null, 1, null, null);
