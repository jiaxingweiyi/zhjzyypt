create table 家政预约.server_condition
(
    id          bigint auto_increment comment '与该家政人员的id一致'
        primary key,
    server_id   int                                          not null comment '家政人员id',
    type        int                                          null comment '类别：2为短期工，1为长期工',
    category_id int                                          null comment '分类id',
    phone       bigint                                       null comment '手机号',
    treatment   varchar(30)                                  null comment '待遇',
    server_time datetime                                     null comment '上岗日期',
    server_area varchar(20)                                  null comment '服务范围',
    salary      float(8, 2)                                  null comment '薪资',
    status      int          default 2                       null comment '状态
公开：0，
私密：1，
未提交：2，
已接单：3，
审核中：4，
审核失败：5
',
    stay        varchar(20)                                  null comment '是否住家 2为否 1为是',
    server_type varchar(10)                                  null comment '家政人员类型',
    area        varchar(10)                                  null comment '地址',
    work_date   varchar(40)                                  null comment '上岗日期',
    tips        varchar(128) default '正在审核中，请耐心等待' null comment '备注',
    introduction   varchar(512) default '无'                    null comment '''家政人员自我介绍'''
)
    comment '状态表';




INSERT INTO 家政预约.server_condition (id, server_id, type, category_id, phone, treatment, server_time, server_area, salary, status, stay, server_type, area, work_date, tips, introduction) VALUES (1, 1, 2, 4, 17825330033, 'sdf ', '2000-10-10 10:10:10', '北京市市辖区丰台区', 400, 0, '1', '长期工', null, '即可上岗 2022-1-1 21:23:23', null, '无');
INSERT INTO 家政预约.server_condition (id, server_id, type, category_id, phone, treatment, server_time, server_area, salary, status, stay, server_type, area, work_date, tips, introduction) VALUES (2, 2, 2, 6, 17825330033, 'aa', '2022-10-10 10:10:10', '天津市市辖区和平区', 340, 4, '2', '长期工', null, '即刻上岗', null, '无');
INSERT INTO 家政预约.server_condition (id, server_id, type, category_id, phone, treatment, server_time, server_area, salary, status, stay, server_type, area, work_date, tips, introduction) VALUES (3, 3, 2, 3, 1355566678, null, null, null, 3, 0, '2', '短期工', null, null, null, '无');
INSERT INTO 家政预约.server_condition (id, server_id, type, category_id, phone, treatment, server_time, server_area, salary, status, stay, server_type, area, work_date, tips, introduction) VALUES (4, 6, 2, 4, 1355566678, null, null, null, 4, 5, '1', '长期工', null, null, '待遇情况填写不清楚', '无');
INSERT INTO 家政预约.server_condition (id, server_id, type, category_id, phone, treatment, server_time, server_area, salary, status, stay, server_type, area, work_date, tips, introduction) VALUES (5, 5, null, 2, 17825330033, '良好', null, '天津市市辖区和平区', 300, 0, '1', '长期工', null, '即刻上岗', null, '无');
INSERT INTO 家政预约.server_condition (id, server_id, type, category_id, phone, treatment, server_time, server_area, salary, status, stay, server_type, area, work_date, tips, introduction) VALUES (6, 8, null, 3, 17825330033, 'wu', null, '北京市市辖区西城区', 400, 1, '2', '短期工', null, '即可上岗', null, '无');




INSERT INTO 家政预约.server_condition (id, server_id, type, category_id, phone, treatment, server_time, server_area, salary, status, stay, server_type, area, work_date, tips) VALUES (1, 1, 2, 4, 1355566678, 'sdf ', '2000-10-10 10:10:10', '广东省', 4, 0, '1', '长期工', null, '即可上岗 2022-1-1 21:23:23', null);
INSERT INTO 家政预约.server_condition (id, server_id, type, category_id, phone, treatment, server_time, server_area, salary, status, stay, server_type, area, work_date, tips) VALUES (2, 2, 2, 1, null, null, '2022-10-10 10:10:10', null, 2, 0, '2', '长期工', null, null, null);
INSERT INTO 家政预约.server_condition (id, server_id, type, category_id, phone, treatment, server_time, server_area, salary, status, stay, server_type, area, work_date, tips) VALUES (3, 3, 2, 3, null, null, null, null, 3, 1, '2', '短期工', null, null, null);
INSERT INTO 家政预约.server_condition (id, server_id, type, category_id, phone, treatment, server_time, server_area, salary, status, stay, server_type, area, work_date, tips) VALUES (4, 6, 2, 4, 1355566678, null, null, null, 4, 1, '1', '长期工', null, null, null);
