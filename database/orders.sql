create table 家政预约.orders
(
    id          bigint auto_increment comment '订单号'
        primary key,
    user_id     int         not null comment '雇主id',
    server_id   int         null comment '家政人员id',
    status      varchar(10) null comment '订单状态',
    create_time datetime    null comment '创建时间',
    server_time datetime    null comment '服务时间',
    total_price float(8, 2) null comment '总价',
    work_date   varchar(20) null comment '上岗日期'
)
    comment '订单表';