create table admin
(
    id           bigint                                                                                                     null,
    user_name    varchar(64)                                                                                                null,
    nick_name    varchar(64)                                                                                                null,
    password     varchar(256)                                                                                                null,
    status       char                                                                                                       null,
    email        varchar(64)                                                                                                null,
    phone_number varchar(32)                                                                                                null,
    sex          char                                                                                                       null,
    avatar       varchar(128) default 'http://ro76gtst3.hn-bkt.clouddn.com/2023/01/13/099d7ecb867a48ecab5e8529c730e673.png' null,
    user_type    char                                                                                                       null,
    create_by    bigint                                                                                                     null,
    create_time  datetime                                                                                                   null,
    update_by    bigint                                                                                                     null,
    update_time  datetime                                                                                                   null,
    del_flag     int                                                                                                        null
)
    comment '管理员表';

INSERT INTO 家政预约.admin (id, user_name, nick_name, password, status, email, phone_number, sex, avatar, user_type, create_by, create_time, update_by, update_time, del_flag) VALUES (2, 'admin', '密码123', '$2a$10$6wOnXuVBASFDyApvWr9jDuUChCG0X7tzZq6PxLvXOi5L1L0s.dVaK', '0', null, null, null, 'http://ro76gtst3.hn-bkt.clouddn.com/2023/01/13/099d7ecb867a48ecab5e8529c730e673.png', '0', null, null, null, null, 0);

insert into 家政预约.admin (id, user_name, nick_name, password, status, email, phone_number, sex, user_type, del_flag) VALUES (1,'yjh','yyyjh','$2a$10$s5Tv74K/5.Sy6/DoDB/vme5rRo0VgKwfFZjmzApaCMlvprt3rsyui','0','y13711710716@163.com','13016078110','f','0','0');