create table category
(
    id          bigint auto_increment
        primary key,
    name        varchar(128)       null comment '分类名',
    pid         bigint default -1  null comment '父分类id，如果没有父分类为-1',
    description varchar(512)       null comment '描述',
    status      char   default '0' null comment '状态0:正常,1其他',
    create_by   bigint             null,
    create_time datetime           null,
    update_by   bigint             null,
    update_time datetime           null,
    del_flag    int    default 0   null comment '删除标志（0代表未删除，1代表已删除）'
)
    comment '分类表';

INSERT INTO 家政预约.category (id, name, pid, description, status, create_by, create_time, update_by, update_time, del_flag) VALUES (1, '保姆陪护', -1, 'wsd', '0', null, null, null, null, 0);
INSERT INTO 家政预约.category (id, name, pid, description, status, create_by, create_time, update_by, update_time, del_flag) VALUES (2, '厨师', -1, 'wsd', '0', null, null, null, null, 0);
INSERT INTO 家政预约.category (id, name, pid, description, status, create_by, create_time, update_by, update_time, del_flag) VALUES (3, '搬家', -1, 'aaa', '0', null, null, null, null, 1);
INSERT INTO 家政预约.category (id, name, pid, description, status, create_by, create_time, update_by, update_time, del_flag) VALUES (4, '母婴护理', -1, null, '0', null, null, null, null, 0);
INSERT INTO 家政预约.category (id, name, pid, description, status, create_by, create_time, update_by, update_time, del_flag) VALUES (5, '保洁', -1, null, '0', null, null, null, null, 0);
INSERT INTO 家政预约.category (id, name, pid, description, status, create_by, create_time, update_by, update_time, del_flag) VALUES (6, '家电维修', -1, null, '0', null, null, null, null, 0);
INSERT INTO 家政预约.category (id, name, pid, description, status, create_by, create_time, update_by, update_time, del_flag) VALUES (18, '不限', -1, 'a''a', '0', null, null, null, null, 0);
