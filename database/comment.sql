create table comment
(
    id            int auto_increment comment '主键id'
        primary key,
    order_id      int            not null comment '相关订单id',
    root_id       int default -1 null comment '根评论id',
    user_id       bigint         not null comment '用户id',
    content       varchar(512)   not null comment '评论内容',
    comment_id    int            null comment '回复人id',
    be_comment_id int            null comment '被回复人id',
    del_flag      int default 0  null comment '逻辑删除标志',
    create_time   datetime       not null comment '评论时间'
)
    comment '评论表';

INSERT INTO 家政预约.comment (id, order_id, root_id, user_id, content, comment_id, be_comment_id, del_flag, create_time) VALUES (1, 1, -1, 1, 'www', null, null, 0, '2023-03-27 02:12:51');
INSERT INTO 家政预约.comment (id, order_id, root_id, user_id, content, comment_id, be_comment_id, del_flag, create_time) VALUES (2, 2, -1, 1, 'zzz', null, null, 0, '2023-03-27 02:14:31');
INSERT INTO 家政预约.comment (id, order_id, root_id, user_id, content, comment_id, be_comment_id, del_flag, create_time) VALUES (3, 1, -1, 2, 'aaa', null, null, 0, '2023-03-27 02:16:51');
INSERT INTO 家政预约.comment (id, order_id, root_id, user_id, content, comment_id, be_comment_id, del_flag, create_time) VALUES (4, 1, -1, 2, 'xxx', null, null, 0, '2023-03-27 21:08:06');
