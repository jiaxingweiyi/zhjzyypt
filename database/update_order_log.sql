CREATE TRIGGER update_order_log
    AFTER UPDATE ON orders
    FOR EACH ROW
BEGIN
    IF NEW.total_price != OLD.total_price THEN
        INSERT INTO order_log(order_id, order_item, log_time)
        VALUES(NEW.id, 'total_price', NOW());
    END IF;
    IF NEW.work_date != OLD.work_date THEN
        INSERT INTO order_log(order_id, order_item, log_time)
        VALUES(NEW.id, 'work_time', NOW());
    END IF;
    IF NEW.server_time != OLD.server_time THEN
        INSERT INTO order_log(order_id, order_item, log_time)
        VALUES(NEW.id, 'server_time', NOW());
    END IF;
END